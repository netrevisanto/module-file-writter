module.exports = {	
	attributes: {		
		appToken: {
			type: 'string',
			defaultsTo: 'd507c5d0-7ebb-11e7-bd79-77d73093898a'
		},
		senhaMaster: {
			type: 'string',
			defaultsTo: undefined
		},
		appTokenINotificacao: {
			type: 'string',
			defaultsTo: ''
		},
		data:{
			type: 'date',
			defaultsTo: '2017-08-11T17:38:05.631Z'
		},
		meuInteiro: {
			type: 'integer',
			defaultsTo: 95912,
		},
		urlINotificacao: {
			type: 'string',
			defaultsTo: "--"
		},
	},
	afterUpdate: function(valores,cb){
		alert("bla bla bla");
	},		
	identidade: "main.configuracao"
}