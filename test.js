var moduleFileWriter = require("./lib/module-file-writter.js");
var lodash = require("lodash");
var uuid = require('uuid/v1');

var retorno = moduleFileWriter.writeToFile(__dirname + "/test/fixture.js", "attributes.appToken.defaultsTo", uuid());
console.log(retorno);
retorno = moduleFileWriter.writeToFile(__dirname + "/test/fixture.js", "attributes.data.defaultsTo", new Date());
console.log(retorno);
retorno = moduleFileWriter.writeToFile(__dirname + "/test/fixture.js", "attributes.meuInteiro.defaultsTo", lodash.random(999,99999));
console.log(retorno);

retorno = moduleFileWriter.writeToFile(__dirname + "/test/fixture.js", "attributes.appTokenINotificacao.defaultsTo", '');
console.log(retorno);
retorno = moduleFileWriter.writeToFile(__dirname + "/test/fixture.js", "attributes.senhaMaster.defaultsTo", undefined);
console.log(retorno);


retorno = moduleFileWriter.writeToFile(__dirname + "/test/fixture.js", "attributes.aaaaaaaa.defaultsTo", undefined);
console.log(retorno);
retorno = moduleFileWriter.writeToFile(__dirname + "/test/abcdef.js", "attributes.a.defaultsTo", undefined);
console.log(retorno);