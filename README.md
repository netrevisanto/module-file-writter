# README #

Easy javascript module manipulation, through the use of RegExp and NodeJS

### How? ###

npm install module-file-writter

### Howdy? ###

var moduleFileWriter = require("module-file-writter");
var retorno = moduleFileWriter.writeToFile(__dirname + "/test/fixture.js", "attributes.appToken.defaultsTo", uuid());
console.log(retorno);

//OUTPUTS
{ status: true, msg: 'File replaced successfuly!' }

Easy!

### Who do I talk to? ###

* Neto Trevisan - netrevisanto.com
* Rodrigo Rodrigues - www.facebook.com/rodrigo.g.rodrigues.7?ref=br_rs