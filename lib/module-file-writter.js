var lodash = require("lodash");
var fs     = require("fs");
module.exports = {
	writeToFile: function(path, objectPath, value){
		var retorno = {};		
		var textoArquivo,modulo;
		try{
			textoArquivo = fs.readFileSync(path, "UTF-8");
			modulo = require(path);			
		} catch(ex){
			retorno.status = false;
			retorno.msg = ex;
			return retorno;
		}		

		var objetoCompleto = lodash.pick(modulo, objectPath);		
		if (lodash.isEmpty(objetoCompleto)){
			retorno.status = false;
			retorno.msg = "could not find the objectPath specified";
			return retorno;
		}
		
		var valorAtual = lodash.get(modulo, objectPath);
		var caminhoSeparado = objectPath.split(".");
		var identificador = lodash.last(caminhoSeparado); // ultimo
		var identificadorPai = lodash.nth(caminhoSeparado, -2); // penultimo		

		var matchValorAtual = valorAtual;
		if (!lodash.isNull(valorAtual) && !lodash.isUndefined(valorAtual)){
			switch(valorAtual.constructor.name){
				case "String": 
				case "Date":
					matchValorAtual = "'" + valorAtual + "'";
				break;
			}	
		}

		var replaceValue = value;
		if (!lodash.isNull(value) && !lodash.isUndefined(value)){
			switch(value.constructor.name){
				case "String": 
					replaceValue = "'" + value + "'";
				break;
				case "Date":
					replaceValue = "'" + value.toISOString() + "'";
				break;
			}	
		}			

		var expressao = new RegExp("("+ identificadorPai +"[\\W\\w]+"+ identificador+": *)(?:"+ matchValorAtual +")");
		textoArquivo = textoArquivo.replace(expressao, "$1" + replaceValue);
		fs.writeFileSync(path,  textoArquivo, "UTF-8");		
		retorno.status = true;
		retorno.msg = "File replaced successfuly!";
		return retorno;
	}
}